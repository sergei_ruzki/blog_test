# coding=utf-8
from django.conf.urls import patterns, include, url
from django.contrib import admin
from blog.views import MyFeed, Feed
from django.contrib.auth.views import login, logout

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/login/$', login, {'template_name': 'login.html'}),
    url(r'^accounts/logout/$', logout, {'next_page':'/',}),
    url(r'^blog/', include('blog.urls')),
    url(r'^$', Feed.as_view(), name='feed' ),
)

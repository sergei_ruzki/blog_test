# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogentry',
            name='owner',
            field=models.ForeignKey(default=1, verbose_name='\u0410\u0432\u0442\u043e\u0440', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='bloguser',
            name='subscriptions',
            field=models.ManyToManyField(related_name='user_subs', verbose_name='\u041f\u043e\u0434\u043f\u0438\u0441\u043a\u0438', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]

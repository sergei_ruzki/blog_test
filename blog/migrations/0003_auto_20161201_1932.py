# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20161130_2021'),
    ]

    operations = [
        migrations.AddField(
            model_name='bloguser',
            name='readed_entries',
            field=models.ManyToManyField(related_name='user_read', verbose_name='\u041f\u0440\u043e\u0447\u0438\u0442\u0430\u043d\u043d\u044b\u0435', to='blog.BlogEntry'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='blogentry',
            name='entry_text',
            field=models.TextField(max_length=50000, verbose_name='\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u0442\u0435\u043a\u0441\u0442 \u0437\u0430\u043f\u0438\u0441\u0438'),
            preserve_default=True,
        ),
    ]

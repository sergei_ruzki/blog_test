# coding=utf-8
from django.db import models
from django.contrib.auth.models import AbstractUser
AbstractUser._meta.get_field('email')._unique = True
AbstractUser._meta.get_field('email').blank = False
from django.db.models.signals import post_save
from django.dispatch import receiver


class NonStaffUserManager(models.Manager):
    def get_queryset(self):
        return super(NonStaffUserManager, self).get_queryset().filter(is_staff=False)


class BlogUser(AbstractUser):
   
    #на данный момент дополнительные данные не используются. Поэтому модифицируем выборку на _не staff_
    #b_objects = NonStaffUserManager()

    subscriptions = models.ManyToManyField('blog.BlogUser', verbose_name=u'Подписки', related_name='user_subs',)
    readed_entries = models.ManyToManyField('blog.BlogEntry', verbose_name=u'Прочитанные', related_name='user_read',)
   
    def __unicode__(self):
            return u'%s' % (self.username)

    class Meta:
        verbose_name = u'пользователь'
        verbose_name_plural = u'пользователи'

    

#класс записей
class BlogEntry(models.Model):
    header = models.CharField(max_length=255, verbose_name=u'Заголовок',)
    entry_text = models.TextField(max_length=50000, verbose_name=u'Введите текст записи',)
    entry_datetime = models.DateTimeField(auto_now_add=True, blank=True, verbose_name=u'Дата и время внесения в базу')
    owner = models.ForeignKey('blog.BlogUser', verbose_name=u'Автор',)

    class Meta:
        verbose_name = u'запись'
        verbose_name_plural = u'записи'

#рассылка уведомлений
#разумеется, ссылка должна быть изменена на нужный домен
@receiver(post_save, sender=BlogEntry)
def send_not(instance, **kwargs):
    if kwargs.pop('created', False):
        mail_list = [u.email for u in instance.owner.subscriptions.all()]        
        if len(mail_list) > 0:
            from django.core.mail import send_mass_mail
            
            emails = []
            for a in mail_list:
                message = (
                    u'Уведомление о новой записи',
                    u'Появилось новое сообщение в блоге, на который Вы подписаны. Ссылка: http://127.0.0.1:8000/blog/view/%s' % (instance.id),
                    u'from@example.com',
                    [unicode(a),],
                    )
                emails.append(email)
            send_mass_mail(emails, fail_silently=False)
            



    



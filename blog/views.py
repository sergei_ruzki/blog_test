# coding=utf-8

from django.shortcuts import render, render_to_response, get_object_or_404
from django.contrib.auth.decorators import login_required 
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import SingleObjectMixin
from django.http import HttpResponseRedirect



from blog.models import BlogEntry, BlogUser

class Feed(ListView):
    model = BlogEntry
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(Feed, self).get_context_data(**kwargs)
        context['feed_type'] = u'feed'
        return context
    def get_queryset(self):
        if self.request.user.is_authenticated():
            from django.db.models import Q
            q_s = BlogEntry.objects.filter(Q(owner__in = self.request.user.subscriptions.all())| Q(owner = self.request.user)).distinct().order_by('-entry_datetime')
            return q_s
        else:
            q_s = BlogEntry.objects.all().order_by('-entry_datetime')
            return q_s


class AllFeed(ListView):
    model = BlogEntry
    paginate_by = 50

    def get_context_data(self, **kwargs):
        context = super(AllFeed, self).get_context_data(**kwargs)
        context['feed_type'] = u'all_feed'
        return context
    def get_queryset(self):
        q_s = BlogEntry.objects.all().order_by('-entry_datetime')
        return q_s

class MyFeed(ListView):
    model = BlogEntry
    paginate_by = 10
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(MyFeed, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MyFeed, self).get_context_data(**kwargs)
        context['feed_type'] = u'my_feed'
        return context
    def get_queryset(self):
        q_s = BlogEntry.objects.filter(owner=self.request.user).order_by('-entry_datetime')
        return q_s

class AuthorFeed(SingleObjectMixin, ListView):
    model = BlogUser
    paginate_by = 10


    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=BlogUser.objects.all())
        return super(AuthorFeed, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AuthorFeed, self).get_context_data(**kwargs)
        
        context['author'] = self.object
        if self.object == self.request.user:
            context['feed_type'] = u'my_feed'
        else:
            context['feed_type'] = u'author_feed'
        return context
    def get_queryset(self):
        q_s = BlogEntry.objects.filter(owner=self.object).order_by('-entry_datetime')
        return q_s


class EntryAdd(CreateView):
    model = BlogEntry
    fields = ['header', 'entry_text']
    success_url = '/'
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EntryAdd, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(EntryAdd, self).form_valid(form)
    

        
    

class EntryEdit(UpdateView):
    model = BlogEntry
    fields = ['header', 'entry_text']
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EntryEdit, self).dispatch(*args, **kwargs)


class EntryDelete(DeleteView):
    model = BlogEntry
    success_url = 'deleted.html'
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EntryDelete, self).dispatch(*args, **kwargs)


class EntryView(DetailView):
    model = BlogEntry
    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        if not obj in self.request.user.readed_entries.all():
            self.request.user.readed_entries.add(obj)
        return super(EntryView, self).get(request, *args, **kwargs)


@login_required
def Subscribe(request, pk=None):
    author = get_object_or_404(BlogUser, id=pk)
    if author in request.user.subscriptions.all():
        request.user.subscriptions.remove(author)
    else:
        request.user.subscriptions.add(author)

    return HttpResponseRedirect(reverse_lazy('author_feed', args=[author.id ]))

@login_required
def Readed(request, pk=None):
    entry = get_object_or_404(BlogEntry, id=pk)
    if request.GET and request.GET['redirect_to']:
        r_path = request.GET['redirect_to']
    if entry in request.user.readed_entries.all():
        request.user.readed_entries.remove(entry)
    else:
        request.user.readed_entries.add(entry)

    return HttpResponseRedirect(r_path)

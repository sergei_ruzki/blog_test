# coding=utf-8
from django import template
import datetime
register = template.Library()

@register.filter
def readed(entry, user):
    if entry in user.readed_entries.all():
        return True
    else:
        return False

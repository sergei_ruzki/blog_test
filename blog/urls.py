# coding=utf-8
from django.conf.urls import patterns, include, url
from django.views.generic.base import RedirectView
from blog.views import EntryAdd, EntryView, EntryEdit, EntryDelete, AuthorFeed, Subscribe, MyFeed, AllFeed


urlpatterns = patterns('',
    url(r'^add/$', EntryAdd.as_view(), name='entry_add'),
    url(r'^b/(?P<pk>\d+)/$', AuthorFeed.as_view(), name='author_feed'),
    url(r'^view/(?P<pk>\d+)/$', EntryView.as_view(), name='entry_view'),
    url(r'^edit/(?P<pk>\d+)/$', EntryEdit.as_view(), name='entry_edit'),
    url(r'^del/(?P<pk>\d+)/$', EntryDelete.as_view(), name='entry_delete'),
    url(r'^sub/(?P<pk>\d+)/$', Subscribe, name='subscribe'),
    url(r'^my/$', MyFeed.as_view(), name = 'my_feed' ),
    url(r'^all/$', AllFeed.as_view(), name = 'all_feed' ),
    url(r'^readed/(?P<pk>\d+)/$', 'blog.views.Readed', name = 'readed' ),
)